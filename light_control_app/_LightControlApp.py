
#spec_example.cpfc.yamlをゴニョゴニョした結果の出力がこうなっててほしい！

import rclpy
from rclpy.node import Node
from rclpy.publisher import Publisher
from rclpy.subscription import Subscription
from std_msgs.msg import Bool, String, Float32, Int32
from diagnostic_msgs.msg import KeyValue
import functools
from typing import List, Callable,TypeVar, Optional
from create_app_tool.ciotpf_app_node_base import CiotpfAppNodeBase, StaticSubscriptionDataModel, ManualSubscriptionDataModel, StaticPublisherDataModel, ManualPublisherDataModel, WildCardSubscriptionDataModel

class _LightControlAppAppNode(CiotpfAppNodeBase):

    APP_NAME = "light_control_app"
    NODE_NAME = "light_control_app_app_node"

    
    def __init__(self) -> None:

        super().__init__(self.NODE_NAME, self.APP_NAME)


        #Subscriptions
        self.static_subscriptions: List[StaticSubscriptionDataModel] = [
            
            StaticSubscriptionDataModel("/ciotpf/app/light_control_app/switch_all", self.switch_controll_subscription_callback, Bool)
        ]
        self.manual_subscriptions= [
            
            ManualSubscriptionDataModel("LightsStatus", "/ciotpf/config/app/light_control_app/manual_topics/subscribe/lights_status", True, self.lights_status_subscription_callback, Bool)
        ]
        
        self.wildcard_subscriptions: List[WildCardSubscriptionDataModel] = [
            
        ]

        #Publishers
        self.static_publishers: List[StaticPublisherDataModel] = [
            
            StaticPublisherDataModel("AllStatus","/ciotpf/app/light_control_app/all_status", String)
        ]
        self.manual_publushers: List[ManualPublisherDataModel] = [
            
            ManualPublisherDataModel("Lights", "/ciotpf/config/app/light_control_app/manual_topics/publish/lights", True, Bool)
        ]

        self.create_static_subscriptions(self.static_subscriptions)
        self.create_wildcard_subscriptions(self.wildcard_subscriptions)
        self.create_static_publishers(self.static_publishers)


    # TIMER_PERIODの間隔で勝手に呼ばれる
    def _periodic_task(self) -> None:
        super()._periodic_task()

        
   #コントロールしてる照明を全て消すor付ける 
    def switch_controll_subscription_callback(self, topicName: str, message: Bool) -> None: 
        raise NotImplementedError() 

    
        
   #コントロールするライトの現在のステータスたち(OnなのかOffなのか) 
    def lights_status_subscription_callback(self, topicName: str, message: Bool) -> None: 
        raise NotImplementedError() 


        

        
    #全ての照明のステータス 
    def all_status_publish(self, value: String) -> None:
        pub: List[StaticPublisherDataModel] = [pub for pub in self.static_publishers if pub.name == "AllStatus"]
        if len(pub) != 1: 
            raise Exception("WHAAAA") 
        self._publish(pub[0], value)
 

        
    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック。 これで、手動でセットした複数のトピックに対してオンオフ操作をする 
    def lights_publish(self, value: Bool) -> None:
        pub: List[ManualPublisherDataModel] = [pub for pub in self.manual_publushers if pub.name == "Lights"]
        if len(pub) != 1: 
            raise Exception("WHAAAA") 
        self._publish(pub[0], value)
 

