import rclpy
from std_msgs.msg import Bool, String, Float32
from diagnostic_msgs.msg import KeyValue
from _LightControlApp import _LightControlAppAppNode


class LightControlAppAppNode(_LightControlAppAppNode):

    
    #全ての照明のステータス 
    #def all_status_publish(self, value: String) -> None:

    
    #コントロール対象の照明が受け付けている、照明のオンオフ操作トピック。 これで、手動でセットした複数のトピックに対してオンオフ操作をする 
    #def lights_publish(self, value: Bool) -> None:



    def __init__(self):
        #親のinitを最初に呼ぶ
        super().__init__()
        ##
        self.light_statuses = {}
        self._timer = self.create_timer(1.0, self.my_periodic)

    
   #コントロールしてる照明を全て消すor付ける 
    def switch_controll_subscription_callback(self, topicName: str, message: Bool) -> None: 
        print("switch_controll ", message.data)
        self.lights_publish(message)
        
    
   #コントロールするライトの現在のステータスたち(OnなのかOffなのか) 
    def lights_status_subscription_callback(self, topicName: str, message: Bool) -> None: 
        self.light_statuses[topicName] = message.data

    def my_periodic(self):
        msg = String()
        msg.data = "controlling light statuses: \n"
        for k in self.light_statuses:
            msg.data +=  k + ": " + str(self.light_statuses[k]) + "\n"
        self.all_status_publish(msg)


    
    



def main(args=None):
    rclpy.init(args=args)

    node = LightControlAppAppNode()

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()