#include <ros2arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>


void temperature_publish(std_msgs::Float32* msg, void* arg);
void pressure_publish(std_msgs::Float32* msg, void* arg);
void humidity_publish(std_msgs::Float32* msg, void* arg);
void luminosity_publish(std_msgs::Float32* msg, void* arg);

void led_light_subscriber_callback(std_msgs::Bool* msg, void* arg);
    
class Esp32EnvironmentSensorDeviceNode: public ros2::Node {
  static constexpr char* NODE_NAME = "esp32_environment_sensor_device_node";
  
  static constexpr int Temperature_PUBLISH_RATE = 1000;
  static constexpr int Pressure_PUBLISH_RATE = 1000;
  static constexpr int Humidity_PUBLISH_RATE = 1000;
  static constexpr int Luminosity_PUBLISH_RATE = 1000;

  public:
    
    ros2::Publisher<std_msgs::Float32>* temperature_publisher = nullptr;
    ros2::Publisher<std_msgs::Float32>* pressure_publisher = nullptr;
    ros2::Publisher<std_msgs::Float32>* humidity_publisher = nullptr;
    ros2::Publisher<std_msgs::Float32>* luminosity_publisher = nullptr;
    
    ros2::Subscriber<std_msgs::Bool>* led_light_subscriber = nullptr;
     
    Esp32EnvironmentSensorDeviceNode(): Node(NODE_NAME) {
      
      temperature_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/esp32_environment_sensor/sensor/temperature");
      this->createWallTimer(Temperature_PUBLISH_RATE, (ros2::CallbackFunc)temperature_publish, nullptr, temperature_publisher);
      pressure_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/esp32_environment_sensor/sensor/pressure");
      this->createWallTimer(Pressure_PUBLISH_RATE, (ros2::CallbackFunc)pressure_publish, nullptr, pressure_publisher);
      humidity_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/esp32_environment_sensor/sensor/humidity");
      this->createWallTimer(Humidity_PUBLISH_RATE, (ros2::CallbackFunc)humidity_publish, nullptr, humidity_publisher);
      luminosity_publisher = this->createPublisher<std_msgs::Float32>("ciotpf/device/esp32_environment_sensor/sensor/luminosity");
      this->createWallTimer(Luminosity_PUBLISH_RATE, (ros2::CallbackFunc)luminosity_publish, nullptr, luminosity_publisher);
      
      this->createSubscriber<std_msgs::Bool>("ciotpf/device/esp32_environment_sensor/actuator/led_light", (ros2::CallbackFunc)led_light_subscriber_callback, nullptr);    
    }
    
};

Esp32EnvironmentSensorDeviceNode* node;
void main_loop();

void MainThread(void *pvParameters){
  while(1){
    main_loop();
  }
}

void loop() 
{
  ros2::spin(node);
}
